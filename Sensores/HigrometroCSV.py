from gpiozero import MCP3008
from time import sleep
import csv

arquivo = open('Higrometro.csv', 'a+')

higro = MCP3008(2) 

while True:
      
      higro_perc = (1 - higro) * 100
      print("Umidade do Solo {0}%".format(higro_perc))
      csv = ("Umidade do Solo {0}% ;\n".format(higro_perc))
      sleep(1)  
      arquivo.write(csv) 