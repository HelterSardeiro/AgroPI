import requests
import json

file = open("clima.csv", "a")
file.write("data,Humidade max,Humidade minima \n")
page = requests.get('http://apiadvisor.climatempo.com.br/api/v1/forecast/locale/3797/days/15?token=518405a79217dfc85f80247b4507addd')
js = json.loads(page.text)
for d in js['data']:
    print('\n{"id":3797,"name":"Pompeia","state":"SP","country":"BR  ') 
    print(d['date'])
    print('-----------------------------------------------------------'+'\nhumidade:')
    print(d['humidity'])
    print('---------------------------------------'+'\ntemperatura de manhã:')
    print('morning:  '+str(d['temperature']['morning']))
    print('temperatura de tarde:')
    print('afternoon:  '+str(d['temperature']['afternoon']))
    print('temperatura de noite:')
    print('night:  '+str(d['temperature']['night']))
    print('---------------------------------------'+'\nchuva:')
    print(d['rain'])
    print('---------------------------------------'+'\nvento:')
    print(d['wind'])
    print('\n#############################################################################################################################################')
    file.write(str( d['date']) + ', ' +str(d['humidity']).replace("{'min':","").replace("'max':","").replace("}","")+'\n')


file.close()
#print(js)
#3797
